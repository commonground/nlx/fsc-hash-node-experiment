Verify FSC hashing algorithm for contracts using Node

## Prerequisites:

- Node v19

## Run the test

```shell
node index.test.js
```

## Reference links

- [FSC MR](https://gitlab.com/commonground/standards/fsc/-/blob/ac55fc23ad5985e16098e2750cf1b4b615aea743/core/draft-fsc-core-00.md)
