import assert from 'node:assert/strict';
import { convertProposalToHash } from './index.js'

const input = {
        contract: {
            hash: {
                hash: 'biBWGKZnIB93AwKRQBEpTY2XTrMJI1BK8h+FXVGWgPAbqg32xLmRInetesJU8HUyvMw9wxvV4xy6iDVgpCrW9g==',
                algorithm: 'HASH_ALGORITHM_SHA3_512',
            },
            proposal: {
                groupId: 'fcs-directory-io',
                period: {
                    start: new Date('2023-11-01 13:00:00Z'),
                    end: new Date('2023-11-29 15:00:00Z'),
                },
                grants: [
                    {
                        type: 'GRANT_TYPE_CONNECTION',
                        connection: {
                            client: {
                                peer: {
                                    subjectSerialNumber: '12345678901234567890',
                                },
                                publicKeyFingerprints: [
                                    'DpAyDYakmVAQ4oOJC3UYLRk/ONRCqMj00TeGJemMiLA=',
                                    '8IfyuP2a0QRJ6SYxWuM/2+ABLuWEiVkI0vrGMWIcB8c=',
                                ],
                            },
                            service: {
                                name: 'postman',
                                peer: {
                                    subjectSerialNumber: '12345678901234567891',
                                },
                            },
                        },
                    },
                    {
                        type: 'GRANT_TYPE_CONNECTION',
                        connection: {
                            client: {
                                peer: {
                                    subjectSerialNumber: '12345678901234567893',
                                },
                                publicKeyFingerprints: [
                                    'DpAyDYakmVAQ4oOJC3UYLRk/ONRCqMj00TeGJemMiLA=',
                                ],
                            },
                            service: {
                                name: 'google',
                                peer: {
                                    subjectSerialNumber: '12345678901234567894',
                                },
                            },
                        },
                    },
                ],
            },
        },
    }

const result = convertProposalToHash(input.contract.proposal)
assert.equal(result, 'biBWGKZnIB93AwKRQBEpTY2XTrMJI1BK8h+FXVGWgPAbqg32xLmRInetesJU8HUyvMw9wxvV4xy6iDVgpCrW9g==')
