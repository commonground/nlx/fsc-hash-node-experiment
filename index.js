import { SHA3 } from 'sha3';

export function convertProposalToHash(proposal) {
    const result = convertFieldValuesToList(proposal)
    const flattened = result.flat(999) // arbitrary depth

    flattened.sort(Buffer.compare)

    const totalLength = flattened.reduce((accumulator, currentValue) => {
        return accumulator + currentValue.length
    }, 0)
    const concatenatedByteArrays = Buffer.concat(flattened, totalLength)

    const hash = new SHA3(512)
    hash.update(concatenatedByteArrays);

    return hash.digest('base64');
}

function isGrantType(input) {
    return [
        'GRANT_TYPE_UNSPECIFIED',
        'GRANT_TYPE_CONNECTION',
        'GRANT_TYPE_DELEGATION',
        'GRANT_TYPE_SERVICE'
    ].includes(input)
}

function convertGrantTypeStringToInt32(input) {
    const buf = Buffer.allocUnsafe(4); // int32

    switch (input) {
        case 'GRANT_TYPE_CONNECTION':
            buf.writeInt32LE(1);
            return buf
        case 'GRANT_TYPE_DELEGATION':
            buf.writeInt32LE(2);
            return buf
        case 'GRANT_TYPE_SERVICE':
            buf.writeInt32LE(3);
            return buf
    }

    buf.writeInt32LE(0);
    return buf
}

function convertFieldValuesToList(input) {
    return Object
        .keys(input)
        .map((key) => {
            const value = input[key]

            switch (typeof value) {
                case "string":
                    if (isGrantType(value)) {
                        return convertGrantTypeStringToInt32(value)
                    }

                    return Buffer.from(`${value}`, 'utf8')


                case "object":
                    if (Object.prototype.toString.call(value) === "[object Date]") {
                        const seconds = Date.parse(value) / 1000

                        const buf = Buffer.allocUnsafe(8); // int64
                        buf.writeBigInt64LE(BigInt(seconds));
                        return buf
                    }

                    if (Array.isArray(value)) {
                        return value.map((item) => {
                            if (typeof item === 'string') {
                                return Buffer.from(`${item}`, 'utf8')
                            }

                            return convertFieldValuesToList(item)
                        })
                    }

                    return convertFieldValuesToList(value)

                default:
                    console.warn(`unsupported instance type '${typeof value}' for value '${value}'`)
                    return ''
            }
        })
}
